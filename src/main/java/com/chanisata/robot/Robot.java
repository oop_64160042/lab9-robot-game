package com.chanisata.robot;

public class Robot extends Unit {

    public Robot(Map map, char symbol, int X, int Y) {
        super(map, symbol, X, Y, false);
    }

    public void Up() {
        int Y = this.getY();
        Y--;
        this.setY(Y);
    }

    public void Down(){
        int Y = this.getY();
        Y++;
        this.setY(Y);
    }

    public void Left(){
        int X = this.getX();
        X--;
        this.setX(X);
    }

    public void Right(){
        int X = this.getX();
        X++;
        this.setX(X);
    }
}
